/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Routes from './src/routes';
import { Provider } from 'react-redux';
import codePush from "react-native-code-push";

import store from './src/store';

// Kiểm tra bản cập nhật bằng thủ công.
let codePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL };
// Kiểm tra bản cập nhật khi app khởi chạy
// let codePushOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_RESUME };

const App = () => {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
};

export default codePush(codePushOptions)(App);
