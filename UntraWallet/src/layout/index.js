import React from 'react';
import {
    SafeAreaView,
    ScrollView,
    View,
    StatusBar,
} from 'react-native';
import {
    Header,
} from 'react-native/Libraries/NewAppScreen';

import { useRoute } from '@react-navigation/native';

import { styles } from './style';

const BaseLayout = ({ children }) => {

    const route = useRoute();

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>
                    {/* <Header title={route.params.name}/> */}

                    <View style={styles.body}>
                        {children}
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
}

export default BaseLayout;