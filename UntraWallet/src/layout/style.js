
import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';
import {
    StyleSheet,
  } from 'react-native';

export const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    body: {
        backgroundColor: Colors.white,
    },
});