import { createSlice } from '@reduxjs/toolkit'

const authSlice = createSlice({
    name: 'posts',
    initialState: {
        isAuth: false,
        password: '123456',
    },
    reducers: {
        login(state, action) {
            state.isAuth = true;
        },
        logout(state, action) {
            state.isAuth = false;
        },
    }
})

// Extract the action creators object and the reducer
const { actions, reducer } = authSlice
// Extract and export each action creator by name
export const { login, logout } = actions
// Export the reducer, either as a default or named export
export default reducer