import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { useSelector } from 'react-redux';

import HomeScreen from '../pages/home';
import LoginScreen from '../pages/login';

const Stack = createStackNavigator();

const Routes = () => {

  const isAuth = useSelector((state) => state.auth.isAuth);

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
      >
        {
          isAuth ?
            <Stack.Screen name="Home" component={HomeScreen} />
            : <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{ headerShown: false }}
            />
        }
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;