import * as React from 'react';

import { View, Text } from 'react-native';

const HistoryScreen = () => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>History Screen</Text>
        </View>
    );
}

export default HistoryScreen;