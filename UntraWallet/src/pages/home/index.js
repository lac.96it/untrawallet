import * as React from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons'

import WalletInfoScreen from '../wallet-info';
import HistoryScreen from '../history';

const Tab = createBottomTabNavigator();

const HomeScreen = () => {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'WalletInfo') {
                        iconName = 'ios-home';
                    } else if (route.name === 'History') {
                        iconName = 'ios-analytics';
                    }

                    // You can return any component that you like here!
                    return <Ionicons name={iconName} size={size} color={color} />;
                },
            })}
        >
            <Tab.Screen
                name="WalletInfo"
                component={WalletInfoScreen}
            />
            <Tab.Screen
                name="History"
                component={HistoryScreen}
            />
        </Tab.Navigator>
    );
}

export default HomeScreen;