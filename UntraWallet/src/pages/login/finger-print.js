import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { login } from '../../store/reducers/auth';

import {
    Image,
    TouchableOpacity,
    AppState
} from 'react-native';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import { FingerprintPopup } from '../../components';
import styles from './styles';

const FingerPrintLogin = () => {
    const [errorMessage, setErrorMessage] = useState();
    const [biometric, setBiometric] = useState();
    const [popupShowed, setPopupShowed] = useState();
    const appState = useRef(AppState.currentState);
    const [appStateVisible, setAppStateVisible] = useState(appState.current);

    const dispatch = useDispatch();

    useEffect(() => {
        AppState.addEventListener('change', handleAppStateChange);
        detectFingerprintAvailable();

        return () => {
            AppState.removeEventListener('change', handleAppStateChange);
        }
    })

    const handleFingerprintShowed = () => {
        if (__DEV__) dispatch(login());
        else setPopupShowed(true);
    };

    const handleFingerprintDismissed = () => {
        setPopupShowed(false);
    };

    const detectFingerprintAvailable = () => {
        FingerprintScanner
            .isSensorAvailable()
            .catch(error => {
                setErrorMessage(error.message);
                setBiometric(error.biometric);
            });
    }

    const handleAppStateChange = (nextAppState) => {
        if (appState.current && appState.current.match(/inactive|background/) && nextAppState === 'active') {
            FingerprintScanner.release();
            detectFingerprintAvailable();
        }

        appState.current = nextAppState;
        setAppStateVisible(appState.current);
    }

    const handleAuthenticate = () => {
        setPopupShowed(false);
        dispatch(login());
    }

    return (
        <>
            {(!errorMessage || __DEV__) && (
                <TouchableOpacity
                    style={styles.fingerprint}
                    onPress={handleFingerprintShowed}
                    disabled={!!errorMessage && !__DEV__}
                >
                    <Image source={require('../../assets/img/finger_print.png')} />
                </TouchableOpacity>
            )}

            {popupShowed && (
                <FingerprintPopup
                    style={styles.popup}
                    handlePopupDismissed={handleFingerprintDismissed}
                    onAuthenticate={handleAuthenticate}
                />
            )}

        </>
    );
}

export default FingerPrintLogin;