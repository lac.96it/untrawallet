import React from 'react';
import {
    Text,
    View,
} from 'react-native';

import FingerPrintLogin from './finger-print';
import LinearGradient from 'react-native-linear-gradient';
import LoginByPassword from './login-by-password';
import { DismissKeyboard } from '../../components';

import styles from './styles';

const LoginScreen = () => {
    return (
        <DismissKeyboard style={{ width: '100%', height: '100%' }}>
            <LinearGradient colors={['#dac915', '#cbd417', '#d2f6fe']} style={styles.container}>
                <View style={styles.wrapper}>
                    <Text style={styles.title}>M __Wallet</Text>
                    <LoginByPassword />
                    <FingerPrintLogin />
                </View>
            </LinearGradient>
        </DismissKeyboard>
    )
}

export default LoginScreen;