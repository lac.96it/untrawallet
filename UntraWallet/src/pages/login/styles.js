import { Dimensions, StyleSheet } from 'react-native';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 0,
  },
  wrapper: {
    position: 'absolute',
    top: '20%',
    bottom: '20%',
    left: '10%',
    right: '10%',
    backgroundColor: '#ffffff',
    borderRadius: 10,
    padding: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.51,
    shadowRadius: 13.16,
    elevation: 20,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  title: {
    fontSize: 42,
    fontWeight: "bold",
    color: "#000",
    textAlign: 'center',
    marginTop: 30
  },
  fingerprint: {
    padding: 20,
    marginVertical: 30,
  },
  popup: {
    width: width * 0.8,
  },
  formLoginPw: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'baseline',
    width: '100%',
  },
  inputPw: {
    borderBottomWidth: 1,
    borderBottomColor: '#d8d8d8',
    height: 38,
    textAlign: 'center',
    marginTop: 60,
    flexGrow: 1,
    fontSize: 16
  }
});