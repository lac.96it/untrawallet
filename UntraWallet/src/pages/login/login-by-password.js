import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { login } from '../../store/reducers/auth';

import { TextInput, View, Pressable, Alert, ActivityIndicator } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'

import styles from './styles';

const LoginByPassword = () => {

    const dispatch = useDispatch();

    const currentPwd = useSelector(state => state.auth.password);

    const [password, setPassword] = useState();
    const [loading, setLoading] = useState(false);

    const handleTextChange = (text) => {
        setPassword(text);
    }

    const handlePressLogin = () => {
        if (loading) return;

        setLoading(true);
        Promise.all([confirmLocalPwd(), waitingForConfirmPwd()])
            .then(([isLoginSuccess]) => {
                if (isLoginSuccess) dispatch(login());
                else Alert.alert('Password incorrect \n Please try again');
            })
            .finally(() => setLoading(false));
    }

    const confirmLocalPwd = () => {
        return new Promise(resolve => password === currentPwd ? resolve(true) : resolve(false));
    }

    const waitingForConfirmPwd = () => {
        return new Promise(resolve => setTimeout(() => resolve(), 500))
    }

    return (
        <View style={styles.formLoginPw}>
            <TextInput
                style={styles.inputPw}
                keyboardType="numeric"
                secureTextEntry
                placeholder="Enter you password"
                onChangeText={handleTextChange}
            />
            <Pressable style={styles.btnLogin} onPress={handlePressLogin}>
                {!loading ?
                    <Ionicons name={'arrow-forward-outline'} size={40} color={'#4c4c4f'} />
                    : <ActivityIndicator />
                }
            </Pressable>
        </View>
    )
}

export default LoginByPassword;