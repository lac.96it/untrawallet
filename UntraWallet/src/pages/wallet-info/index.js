import * as React from 'react';
import { useDispatch } from 'react-redux';
import { logout } from '../../store/reducers/auth';

import { View, Text, Button } from 'react-native';


const WalletInfoScreen = () => {

    const dispatch = useDispatch();

    const gotoLogin = () => dispatch(logout());

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Wallet Info Screen</Text>
            <Button
                title="Logout"
                onPress={gotoLogin}
            />
        </View>
    );
}

export default WalletInfoScreen;