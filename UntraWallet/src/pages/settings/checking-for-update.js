import React from 'react';
import { View, Button } from 'react-native';

const CheckingForUpdate = () => {

    const handleChecking = () => {
        codePush.sync({
            updateDialog: true,
            installMode: codePush.InstallMode.IMMEDIATE
        });
    }

    return (
        <View>
            <Button onPress={handleChecking}>Check for update</Button>
        </View>
    )
}

export default CheckingForUpdate;